<?php

namespace Drupal\domain\Drush\Commands;

/**
 * Placeholder class for exceptions.
 */
class DomainCommandException extends \Exception {

}
